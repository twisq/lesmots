package nl.twisq.lesmots;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = MotsController.class)
public class LesmotsApplication {

	public static void main(String[] args) {
		SpringApplication.run(LesmotsApplication.class, args);
	}

}
