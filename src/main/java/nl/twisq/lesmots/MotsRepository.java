package nl.twisq.lesmots;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MotsRepository extends MongoRepository<Mot, String> {
  Mot findBy_id(ObjectId _id);
}