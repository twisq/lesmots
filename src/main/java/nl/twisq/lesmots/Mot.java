package nl.twisq.lesmots;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "fr")
public class Mot {
  @Id
  public ObjectId _id;
  
  public String fr;
  public String nl;
  public String type;
  
  // Constructors
  public Mot() {}
  
  public Mot(ObjectId _id, String fr, String nl, String type) {
    this._id = _id;
    this.fr = fr;
    this.nl = nl;
    this.type = type;
  }
  
  // ObjectId needs to be converted to string
  public String get_id() { return _id.toHexString(); }
  public void set_id(ObjectId _id) { this._id = _id; }
  
  public String getFr() { return fr; }
  public void setFr(String fr) { this.fr = fr; }
  
  public String getNl() { return nl; }
  public void setNl(String nl) { this.nl = nl; }
  
  public String getType() { return type; }
  public void setType(String type) { this.type = type; }
}
