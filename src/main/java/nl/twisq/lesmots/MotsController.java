package nl.twisq.lesmots;

import java.util.List;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MotsController {

    @Autowired
    private MotsRepository repository;

    @RequestMapping("/test")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @RequestMapping(value = "/mots", method = RequestMethod.GET)
    @ResponseBody
    public List<Mot> getAllPets() {
    return repository.findAll();
    }

    @RequestMapping(value = "/mots/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Mot getPetById(@PathVariable("id") ObjectId id) {
    return repository.findBy_id(id);
    }

    @RequestMapping(value = "/mots/{id}", method = RequestMethod.PUT)
    public void modifyPetById(@PathVariable("id") ObjectId id, @Valid @RequestBody Mot mot) {
    mot.set_id(id);
    repository.save(mot);
    }

    @RequestMapping(value = "/mots", method = RequestMethod.POST)
    public Mot createPet(@Valid @RequestBody Mot mot) {
    mot.set_id(ObjectId.get());
    repository.save(mot);
    return mot;
    }

    @RequestMapping(value = "/mots/{id}", method = RequestMethod.DELETE)
    public void deletePet(@PathVariable ObjectId id) {
    repository.delete(repository.findBy_id(id));
    }

}
