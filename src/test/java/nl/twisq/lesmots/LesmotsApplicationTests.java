package nl.twisq.lesmots;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LesmotsApplicationTests {

	@Test
	public void contextLoads() {
	}

}
